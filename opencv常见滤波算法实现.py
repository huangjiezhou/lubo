# -*- coding:utf-8 -*-
#本程序用于将一张彩色图片分解成BGR的分量显示，灰度图显示，HSV分量显示
import cv2  #导入opencv模块
import numpy as np

print("Hellow word!")     #打印“Hello word！”，验证模块导入成功

img = cv2.imread("imag3.jpg")  #导入图片，图片放在程序所在目录
cv2.namedWindow("imagshow", 2)   #创建一个窗口
cv2.imshow('imagshow', img)    #显示原始图片

# 均值滤波
img_mean = cv2.blur(img, (3,3)) #参数1输入图像，参数2核大小
cv2.namedWindow("mean", 2)   #创建一个窗口
cv2.imshow('mean', img_mean)    #显示原始图片

# 高斯滤波
img_Guassian = cv2.GaussianBlur(img,(3,3),0)
cv2.namedWindow("Guassian", 2)   #创建一个窗口
cv2.imshow('Guassian', img_Guassian)    #显示原始图片

# 中值滤波
img_median = cv2.medianBlur(img, 5)
cv2.namedWindow("median", 2)   #创建一个窗口
cv2.imshow('median', img_median)    #显示原始图片

# 双边滤波
img_bilater = cv2.bilateralFilter(img,9,75,75)
cv2.namedWindow("bilater", 2)   #创建一个窗口
cv2.imshow('bilater', img_bilater)    #显示原始图片

cv2.waitKey()